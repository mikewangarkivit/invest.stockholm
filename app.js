const path = require('path');
const fs = require('fs');
const url = require('url');
const file_management = require("../arkiweraengine/app/file.management");
const { Arkiwera_Engine, WebFile } = require('../arkiweraengine/app/arkiwera.engine.js');
const { sleep } = require("../arkiweraengine/app/sleep.js");


console.log('=================================');
console.log('=        Process starts....     =');
console.log('=================================');

// main code

let b_database = false;
(async () => {
    try {
        let config = {};
        if (fs.existsSync(__dirname + path.sep + 'config.json')) {
            const json = fs.readFileSync(__dirname + path.sep + 'config.json').toString();
            config = JSON.parse(json);
            config.root = path.resolve(__dirname + path.sep);
            config.path = path.resolve(__dirname + path.sep + 'config.json');
            // file_management.saveJsonObjectToFile(config.path, config);
        } else {
            throw 'Saknar ' + __dirname + path.sep + 'config.json';
        }
        /**
         * Create Database if the databasename isn't exists.
         * OMR can't create database
         */
        console.log('\r\nCreate/Initialize Database...');
        const DB = require('..' + path.sep + 'arkiweraengine' + path.sep + 'app' + path.sep + 'db');
        const con = new DB(config.mysql);
        b_database = await con.initialize(config.mysql)
        if (b_database) {
            console.log('Databasen är redo.');
        } else {
            console.log('Databasen är inte redo. Exit at once!');
            process.exit();
        }

        /**
         * Create ORM object
         */
        console.log('\r\nCreate/Initialize ORM Object ...');
        const ORM = require('..' + path.sep + 'arkiweraengine' + path.sep + 'app' + path.sep + 'orm');
        let orm = new ORM(config.mysql);
        b_databse = await orm.initialize(config.mysql);
        if (b_database) {
            console.log('ORM är redo.');
        } else {
            console.log('ORM är inte redo. Exit at once!');
            process.exit();
        }


        /**
         * open Puppeteer.
         */
        if (config.domain.save_directory == undefined) {
            config.domain.save_directory = __dirname + path.sep + "downloads" + path.sep;
        } else {
            config.domain.save_directory = path.resolve(__dirname + path.sep + config.domain.save_directory) + path.sep; //säkerställa att save_directory slutar på "/"
        }

        const args = require('minimist')(process.argv.slice(2))
        let is_need_convert = true;
        let is_need_download = true;
        console.log("\r\nparameter:", args);
        if (args['action'] != undefined) {
            const actions = args['action'].toLowerCase().split('|');
            for (let index = 0; index < actions.length; index++) {
                const element = actions[index];
                if (element == 'noconvert') {
                    is_need_convert = false;
                }
                if (element == 'nodownload') {
                    is_need_download = false;
                }

            }
        }
        console.log("need to convert:", is_need_convert);
        console.log("need to download:", is_need_download);
        const arkiwera_engine = new Instance(config, orm);
        await arkiwera_engine.initialize_Engine();

        if (is_need_download) {
            console.log('start download...');
            await arkiwera_engine.download();
        }
        if (is_need_convert) {
            console.log('start convert...');
            await arkiwera_engine.convert();
        }
        // console.log(result);
        await arkiwera_engine.dispose();
        /**
         * close ORM
         */
        await orm.close();

    } catch (err) {
        console.log(err);
    } finally {
        console.log('=================================');
        console.log('=        Process ends ....      =');
        console.log('=================================');
        process.exit();
    }
})();


class Instance extends Arkiwera_Engine {
    constructor(config, orm, login = null) {
        super(config, orm, login);
    }
    async login(defaultUrl, userName, password) {
        // super.login(defaultUrl, userName, password);
        // console.log('class Instance');
        return true;
    }

    /**
     * skriva över den här funktionen.
     * den första sidan behöver några musklick för att hämta mer nyheter och sedan får man spara hela sidan.
     * @param {} PuppeteerPage 
     * @returns 
     */
    async execute_extra(PuppeteerPage, checkUrl) {


    }
    // letar efter <script> windows.__Model ="***" </scirpt>
    async convert_special(fileName) {
        // delete <base href="/">

        return true;
    }
    is_blacklist(url) {
        return false;
    }
} // end of class Instance